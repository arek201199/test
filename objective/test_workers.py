import unittest
from workers import Worker
w=Worker()
class test_workers(unittest.TestCase):
#Sprawdzamy, czy po usunięciu pracownika jest mniejsza, niż przed usunięciem.
	def test_deleteWorker_countWorkers_should_be_less_than_before(self):
		w.addWorker("Janusz", "Kowalski", "Kowal")
		count_before = w.numWorkers()
		w.deleteWorker(0)
		count_after=w.numWorkers()
		self.assertLess(count_after, count_before)
#Sprawdzmy, czy po dodaniu pracownika liczba pracowników jest większa, niż przed dodaniem.
	def test_addWorker_numWorkers_should_be_greater_than_before(self):
		count_before = w.numWorkers()
		w.addWorker("Roman", "Januszkiewicz", "Kierownik")
		self.assertGreater(w.numWorkers(), count_before)

unittest.main()
