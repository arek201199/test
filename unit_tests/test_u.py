import unittest
from testunit import testing
class test(unittest.TestCase):
#Sprawdzamy, czy podając 0 w parametrze funkcja zwraca 0
	def test_is_param_not_greater_and_less_than_zero(self):
		t = testing()
		self.assertEqual(t.calcsth(0), 0)
#Sprawdzamy, czy podając liczbę ujemną funkcja zwraca -1
	def test_is_param_less_than_zero(self):
		t = testing()
		self.assertEqual(t.calcsth(-3), -1)
#Sprawdzamy, czy funkcja dla parametru większego od zera zwraca 1
	def test_is_param_greater_than_zero(self):
		t = testing()
		self.assertEqual(t.calcsth(5), 1)

if __name__ == '__main__':
	unittest.main()
