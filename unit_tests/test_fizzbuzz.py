import unittest
from fizzbuzz import fb
class test_fizzbuzz(unittest.TestCase):
#Sprawdzamy, czy valReturn zwraca Fizz dla wieloktorności trójek
	def test_valReturn_should_return_fizz_for_three_multiples(self):
		f = fb()
		for i in range(3, 100, 3):
			if  i % 5 != 0:
				val_return = f.valReturn(i)
				print("i: {}, valReturn:{}".format(i,val_return))
				self.assertEqual(val_return, "Fizz")
#Sprawzdamy, czy valReturn zwraca Buzz dla wielokrotności piątek
	def test_valReturn_should_return_buzz_for_five_multiples(self):
		f = fb()
		for i in range(5, 100, 5):
			if i % 3 != 0:
				val_return = f.valReturn(i)
				print("i: {}, valReturn:{}".format(i,val_return))
				self.assertEqual(val_return, "Buzz")
#Sprawdzamy, czy valReturn zwraca FizzBuzz dla wielokrotności trójek i piątek
	def test_valReturn_should_return_fizzbuzz_for_three_and_five_multiples(self):
		f=fb()
		for i in range(15, 100, 15):
			if i % 5 == 0 and i % 3 == 0:
				val_return = f.valReturn(i)
				print("i: {}, valReturn:{}".format(i, val_return))
				self.assertEqual(val_return, "FizzBuzz")


unittest.main()

